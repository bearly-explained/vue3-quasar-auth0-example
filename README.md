# vue3-quasar-auth0-example

This repository provides an example of a Quasar based Vue3 frontend using Auth0.

The tutorial [Introducing the Brand-New Auth0 Vue SDK](https://auth0.com/blog/introducing-auth0-vue-sdk/) which
covers the [auth0-vue](https://github.com/auth0/auth0-vue) package is adapted to include Quasar, and provide an example
of how additional data such as roles can be included.

This authentication flow is able to "protect" the frontend - but does not in any way communicate with a backend. As such
it can be useful for identification, but in itself would not be sufficient to secure a system. Securing both the frontend
and backend using Auth0 will be covered in another repository.

## Technologies and tooling used

### Quasar

The [Quasar](https://quasar.dev/) framework is used to provide styled and configurable components. For further details
see [Why Quasar?](https://quasar.dev/introduction-to-quasar)

## Pre-requisites (Auth0)

From the [Auth0 Dashboard](https://manage.auth0.com/dashboard), create a new Application:

- Applications -> Applications -> Create Application
- Provide a name
- Select `Single Page Web Applications`

The steps provided are available following the Vue3 QuickStart, for reference, from `Settings`:
- Note the `Domain` and `Client ID` - these will be used for the .env file later
- Populate `Allowed Callback URLs`, `Allowed Logout URLs`, `Allowed Web Origins` with: http://localhost:8081

Now add a Rule to return the Roles:
- Auth Pipeline -> Rules -> Create
- Select `Empty rule`
- Provide a name e.g. `Roles`
- Enter the Script as below:

For actual usage, use an appropriate domain instead of `somewhere.com` (the `IndexPage.vue` will need to be updated
accordingly as well).

```javascript
function (user, context, callback) {
  const namespace = 'https://somewhere.com';
  const assignedRoles = (context.authorization || {}).roles;

  let idTokenClaims = context.idToken || {};
  let accessTokenClaims = context.accessToken || {};

  idTokenClaims[`${namespace}/roles`] = assignedRoles;
  accessTokenClaims[`${namespace}/roles`] = assignedRoles;

  context.idToken = idTokenClaims;
  context.accessToken = accessTokenClaims;

  callback(null, user, context);
}
```
## Pre-requisites (frontend)

For convenience, the following commands to install pre-requisites can be performed by calling the script
`setup_prerequisites.sh`

### nvm / node

Make sure we are using an up-to-date version of node, using [nvm](https://github.com/nvm-sh/nvm)

```shell
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
source ~/.bashrc
nvm install --lts
nvm use --lts
```

### Yarn

For Node.js >= 16.10, Yarn can be managed via "Corepack". See [Yarn Installation](https://yarnpkg.com/getting-started/install)

To make available, on a PATH all you should need to do is:
```shell
corepack enable
```

Verify the yarn binary is available via the current $PATH, using the node version managed by nvm:
```shell
$ which yarn
/home/mgeeves/.nvm/versions/node/v16.14.2/bin/yarn
```

### Quasar

Install the latest quasar, to make the quasar binary available for managing projects

```shell
yarn global add @quasar/cli@latest
```

## Setup

### Frontend

Create the initial Quasar project, in the fastapi-vue3-auth-example folder:

```shell
yarn create quasar
```

The following choices were made for the Quasar setup:

| Question                                   | Choice                                                               |
|--------------------------------------------|----------------------------------------------------------------------|
| What would you like to build?              | App with Quasar CLI, let's go!                                       |
| Project folder                             | frontend                                                             |
| Pick Quasar version                        | Quasar v2                                                            |
| Pick script type                           | Typescript                                                           |
| Pick Quasar App CLI variant                | Quasar App CLI with Vite (BETA stage)                                |
| Package name                               | frontend                                                             |
| Project product name                       | Vue3 Auth0 Example                                                   |
| Project description                        | A Quasar project using Vue3, demonstrating authentication with Auth0 |
| Author                                     | Mike Geeves <mike.geeves@bearly-compiling.com>                       |
| Pick a Vue component style                 | Composition API                                                      |
| Pick your CSS preprocessor                 | Sass with SCSS syntax                                                |
| Check the features needed for your project | ESLint                                                               |
| Pick an ESLint preset                      | Prettier                                                             |
| Install project dependencies?              | Yes, use yarn                                                        |

Now from the `frontend` dir:
```shell
cd frontend
```

To be able to pull config from a .env file, add dotenv

```shell
yarn add --dev dotenv
```

Add the Auth0 Vue3 SDK:

```shell
yarn add @auth0/auth0-vue
```

Once installation has completed, as per the Quasar setup guidance, from the `frontend` dir:

```shell
quasar dev --port 8081
```

This will perform a development build, serve it up, and open the root page in a browser.

Note: port 8081 is being specified. If the port is busy, another port will be used. The port will need to match up with
the one provided for Auth0, or the authentication flow will fail.

# Configuration

Create a `.env` file in the `frontend` dir containing the following:
```text
AUTH0_DOMAIN=<YOUR AUTH0 DOMAIN>
AUTH0_CLIENT_ID=<YOUR AUTH0 CLIENT ID>
```

# Adding in the frontend

## Tidying up

We won't be using some of the example code generated by Quasar, so the following
files have been removed, along with corresponding references:

- src/components/ExampleComponent.vue
- src/components/models.ts

# Auth0

A component needs to be wired in for Auth0, for this we are adding
`src/boot/auth0.ts`. For Quasar to load this, we need to add `auth0` to the
`quasar.config.js file:

```javascript
boot: [
      'auth0'
]
```
To pull in the domain and client_id, include these via adding to the
`quasar.config.js` file, the `env` parameter to the `build` section:

```javascript
build: {
  env: require('dotenv').config().parsed
}
```

# Usage

## Initial

Once running with the domain and client_id provided, the initial page will indicate the user is not logged in:
![not_logged_in.png](not_logged_in.png)

## Login

Clicking the Login button should take you to an auth0 login page, where you will need to either sign up, providing a
username/password to Auth0, or sign in using a Google account.

To begin with, your user will have an empty list of roles:
![no_role.png](no_role.png)

## Add Role

From the [Auth0 Dashboard](https://manage.auth0.com/dashboard), you should be able to find the user created, and add
them to a Role:

- User Management -> Roles -> Create Role
- Provide the name `Permitted User` and any description e.g. `Users who are allowed to access the system`
- Users -> Add Users
- Search for your username and `Assign`

Reloading the page should now indicate you have the assigned role:
![have_role.png](have_role.png)

Additional information is displayed at the bottom, showing the Auth0 provided `user`, `claims`, and the derived `roles`
which are in a Vue `computed` function.