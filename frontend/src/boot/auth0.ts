import {boot} from 'quasar/wrappers';
import {createAuth0} from '@auth0/auth0-vue';

export default boot(({app}) => {
  console.log('-> auth0 boot');
  console.log('AUTH0_DOMAIN:', process.env.AUTH0_DOMAIN)
  console.log('AUTH0_CLIENT_ID:', process.env.AUTH0_CLIENT_ID)
  const auth0 = createAuth0({
    domain: process.env.AUTH0_DOMAIN ?? 'DOMAIN NOT PROVIDED',
    client_id: process.env.AUTH0_CLIENT_ID ?? 'CLIENT_ID NOT PROVIDED',
    redirect_uri: window.location.origin,
    audience: 'bearly-under-control'
  });
  app.use(auth0);

  console.log('<- auth0 boot');
});

export {createAuth0};
