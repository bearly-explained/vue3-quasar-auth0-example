#!/bin/bash

# Install/upgrade nvm, and the latest LTS node
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash

# nvm will have been added to the .bashrc, making it available on the $PATH
# Source the same script to make the command available here
export NVM_DIR=$HOME/.nvm;
source $NVM_DIR/nvm.sh;

nvm install --lts
nvm use --lts

# Make yarn available
corepack enable

# Make quasar available
yarn global add @quasar/cli@latest
